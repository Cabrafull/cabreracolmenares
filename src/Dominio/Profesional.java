//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Profesional extends Cuenta {

    //Atributos
    private String nombreTituloProfesional;
    private String fechaDeGraduacion;
    private String paisDondeObtuvoTitulo;
    private ArrayList<Consulta> listaConsultasNoContestadas;

    //Constructores
    public Profesional() {
        super();
        this.nombreTituloProfesional = "";
        this.fechaDeGraduacion = "";
        this.paisDondeObtuvoTitulo = "";
        this.listaConsultasNoContestadas = new ArrayList<>();
    }

    public Profesional(String laCuenta, BufferedImage laFotoPerfil, String elNombre, String elNombre2,
            String elApellido, String elApellido2) {
        super(laCuenta, laFotoPerfil, elNombre, elNombre2, elApellido, elApellido2);
        this.setNombreTituloProfesional("");
        this.setFechaDeGraduacion("");
        this.setPaisDondeObtuvoTitulo("");
        this.listaConsultasNoContestadas = new ArrayList<>();
    }
    
    //Gets y Sets
    public String getNombreTituloProfesional() {
        return nombreTituloProfesional;
    }

    public void setNombreTituloProfesional(String unNombreTituloProfesional) {
        this.nombreTituloProfesional = unNombreTituloProfesional;
    }

    public String getFechaDeGraduacion() {
        return fechaDeGraduacion;
    }

    public void setFechaDeGraduacion(String unaFechaDeGraduacion) {
        this.fechaDeGraduacion = unaFechaDeGraduacion;
    }

    public String getPaisDondeObtuvoTitulo() {
        return paisDondeObtuvoTitulo;
    }

    public void setPaisDondeObtuvoTitulo(String unPaisDondeObtuvoTitulo) {
        this.paisDondeObtuvoTitulo = unPaisDondeObtuvoTitulo;
    }

    public ArrayList<Consulta> getListaConsultasNoContestadas() {
        return listaConsultasNoContestadas;
    }

    public void setListaConsultasNoContestadas(ArrayList<Consulta> unaListaConsultasNoContestadas) {
        this.listaConsultasNoContestadas = unaListaConsultasNoContestadas;
    }

    //Redefinir el ToString
    @Override
    public String toString() {
        String retorno = "";
        if (!this.getNombre().equals("")) {
            retorno = retorno + this.getNombre() + " ";
        }
        if (!this.getNombre2().equals("")) {
            retorno = retorno + this.getNombre2() + " ";
        }
        if (!this.getApellido().equals("")) {
            retorno = retorno + this.getApellido() + " ";
        }
        if (!this.getApellido2().equals("")) {
            retorno = retorno + this.getApellido2();
        }
        return retorno;
    }
}
