//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import Interfaz.VentanaMenuPrincipal;

public class Aplicacion {
    public static void main(String[] args) {
        Sistema unSistema = new Sistema();
        Interfaz.VentanaMenuPrincipal vent = new VentanaMenuPrincipal(unSistema);
        vent.setVisible(true);
    }
}
