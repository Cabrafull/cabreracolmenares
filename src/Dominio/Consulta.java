//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

public class Consulta {

    //Atributos
    private String consulta;
    private boolean contestada;
    private Alimento alimento;
    private Usuario cliente;
    private String idProfesional;

    //Constructores
    public Consulta() {
        this.consulta = "";
        this.contestada = false;
        this.alimento = null;
        this.cliente = null;
        this.idProfesional = "";
    }

    public Consulta(String unaConsulta, boolean estaContestada, Alimento unAlimento, Usuario unCliente, String laIdProfesional) {
        this.setConsulta(unaConsulta);
        this.setContestada(estaContestada);
        this.setAlimento(unAlimento);
        this.setCliente(unCliente);
        this.setIdProfesional(laIdProfesional);
    }

    //Gets y Sets
    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String unaConsulta) {
        this.consulta = unaConsulta;
    }

    public boolean getContestada() {
        return contestada;
    }

    public void setContestada(boolean estaContestada) {
        this.contestada = estaContestada;
    }

    public Alimento getAlimento() {
        return alimento;
    }

    public void setAlimento(Alimento unAlimento) {
        this.alimento = unAlimento;
    }

    public Usuario getCliente() {
        return cliente;
    }

    public void setCliente(Usuario unCliente) {
        this.cliente = unCliente;
    }

    public String getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(String laIdProfesional) {
        this.idProfesional = laIdProfesional;
    }

    //Redefinir el ToString
    @Override
    public String toString() {
        return "Alimento " + this.getAlimento().getNombre() + " por profesional " + this.getIdProfesional();
    }

    //Redefinir el equals
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        return this.getIdProfesional().equals(((Consulta) o).getIdProfesional()) &&
                this.getAlimento().equals(((Consulta) o).getAlimento());
    }
}
