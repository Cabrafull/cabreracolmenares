//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.util.ArrayList;

public class Alimento {

    //Atributos
    private String nombre;
    private String tipoDeAlimento;
    private ArrayList<Nutriente> listaNutrientesPrincipales;

    //Constructores
    public Alimento() {
        this.nombre = "";
        this.tipoDeAlimento = "";
        this.listaNutrientesPrincipales  = new ArrayList<>();
    }

    public Alimento(String unNombre, String unTipoDeAlimento) {
        this.nombre = unNombre;
        this.tipoDeAlimento = unTipoDeAlimento;
        this.listaNutrientesPrincipales  = new ArrayList<>();
    }

    //Gets y Sets
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }

    public String getTipoDeAlimento() {
        return tipoDeAlimento;
    }

    public void setTipoDeAlimento(String unTipoDeAlimento) {
        this.tipoDeAlimento = unTipoDeAlimento;
    }

    public ArrayList<Nutriente> getListaNutrientesPrincipales() {
        return listaNutrientesPrincipales;
    }

    public void setListaNutrientesPrincipales(ArrayList<Nutriente> unaListaNutrientesPrincipales) {
        this.listaNutrientesPrincipales = unaListaNutrientesPrincipales;
    }

    //Redefinir el ToString
    @Override
    public String toString() {
        if (this == null) {
            return "";
        }
        return this.getNombre() + " (" + this.getTipoDeAlimento() + ")";
    }

    //Redefinir el equals
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        return this.getNombre().equalsIgnoreCase(((Alimento) o).getNombre());
    }
}
