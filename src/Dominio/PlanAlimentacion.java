//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

public class PlanAlimentacion {
    
    //Atributos
    private Alimento[][] alimentos;
    private String nombre;

    //Constructores
    public PlanAlimentacion() {
        this.alimentos = new Alimento[7][4];
        this.nombre = "";

    }

    public PlanAlimentacion(Alimento[][] losAlimentos, String elNombre) {
        this.setAlimentos(losAlimentos);
        this.setNombre(elNombre);
    }

    //Gets y Sets
    public Alimento[][] getAlimentos() {
        return alimentos;
    }

    public void setAlimentos(Alimento[][] losAlimentos) {
        this.alimentos = losAlimentos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String elNombre) {
        this.nombre = elNombre;
    }

    //Redefinir el ToString
    @Override
    public String toString() {
        return this.getNombre();
    }
}
