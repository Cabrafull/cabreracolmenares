//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.awt.image.BufferedImage;

public class Cuenta {
    //Atributos
    private String cuenta;
    private String nombre;
    private String nombre2;
    private String apellido;
    private String apellido2;
    private String fechaDeNacimiento;
    private BufferedImage fotoPerfil;

    //Constructores
    public Cuenta() {
        this.cuenta = "";
        this.nombre = "";
        this.nombre2 = "";
        this.apellido = "";
        this.apellido2 = "";
        this.fechaDeNacimiento = "";
        this.fotoPerfil = null;
    }

    public Cuenta(String laCuenta, BufferedImage laFotoPerfil, String elNombre, String elNombre2,
            String elApellido, String elApellido2) {
        this.setCuenta(laCuenta);
        this.setFotoPerfil(laFotoPerfil);
        this.setNombre(elNombre);
        this.setNombre2(elNombre2);
        this.setApellido(elApellido);
        this.setApellido2(elApellido2);
        this.setFechaDeNacimiento("");
    }

    //Gets y Sets
    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String unaCuenta) {
        this.cuenta = unaCuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String unNombre2) {
        this.nombre2 = unNombre2;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String unApellido) {
        this.apellido = unApellido;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String unApellido2) {
        this.apellido2 = unApellido2;
    }

    public String getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(String unaFechaDeNacimiento) {
        this.fechaDeNacimiento = unaFechaDeNacimiento;
    }

    public BufferedImage getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(BufferedImage laFotoPerfil) {
        this.fotoPerfil = laFotoPerfil;
    }

    //Redefinir el equals
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        return this.getCuenta().equalsIgnoreCase(((Cuenta) o).getCuenta());
    }
}
