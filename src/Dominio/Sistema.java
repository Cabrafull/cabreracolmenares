//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.util.ArrayList;

public class Sistema {

    //Atributoss
    private ArrayList<Usuario> listaUsuarios;
    private ArrayList<Profesional> listaProfesionales;
    private ArrayList<Alimento> listaAlimentos;

    //Constructores
    public Sistema() {
        this.listaUsuarios = new ArrayList<>();
        this.listaProfesionales = new ArrayList<>();
        this.listaAlimentos = new ArrayList<>();
    }

    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(ArrayList<Usuario> unaListaUsuarios) {
        this.listaUsuarios = unaListaUsuarios;
    }

    public ArrayList<Profesional> getListaProfesionales() {
        return listaProfesionales;
    }

    public void setListaProfesionales(ArrayList<Profesional> unaListaProfesionales) {
        this.listaProfesionales = unaListaProfesionales;
    }

    public ArrayList<Alimento> getListaAlimentos() {
        return listaAlimentos;
    }

    public void setListaAlimentos(ArrayList<Alimento> unaListaAlimentos) {
        this.listaAlimentos = unaListaAlimentos;
    }

    public boolean hayMismaCuentaUsuario(String cuenta) {
        boolean retorno = false;
        for (int i = 0; i < this.getListaUsuarios().size() && !retorno; i++) {
            retorno = this.getListaUsuarios().get(i).getCuenta().equals(cuenta);
        }
        return retorno;
    }
    
    public boolean hayMismaCuentaProfesional(String cuenta) {
        boolean retorno = false;
        for (int i = 0; i < this.getListaProfesionales().size() && !retorno; i++) {
            retorno = this.getListaProfesionales().get(i).getCuenta().equals(cuenta);
        }
        return retorno;
    }
    
    public boolean hayMismaCuentaSistema(String cuenta) {
        return hayMismaCuentaUsuario(cuenta) || hayMismaCuentaProfesional(cuenta);
    }
    
    public Alimento getAlimentoConNombre(String nombreAlimento) {
        for (int i = 0; i < this.getListaAlimentos().size(); i++) {
            if (this.getListaAlimentos().get(i).getNombre().equalsIgnoreCase(nombreAlimento)) {
                return this.getListaAlimentos().get(i);
            }
        }
        return null;
    }
}
