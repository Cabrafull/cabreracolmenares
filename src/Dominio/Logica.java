//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.io.File;

public class Logica {

    public static boolean esStringCorrecto(String aVerificar) {
        boolean retorno = true;
        if (aVerificar.equals("")) {
            return true;
        }
        for (int i = 0; i < aVerificar.length(); i++) {
            char aVerificarEnI = aVerificar.charAt(i);
            int let = (int) aVerificarEnI;
            //Si no esta dentro de lo correcto sea correcto solo numeros y letras (0-9,a-z,A-Z)
            if (!((let > 47 && let < 58) || (let > 64 && let < 91) || (let > 96 && let < 123))) {
                //Ñ, ñ, tildes
                if (!((let==241)||(let==209)||(let==225)||(let==233)||(let==237)||(let==243)||(let==250)
                        ||(let==193)||(let==201)||(let==205)||(let==211)||(let==218))) {
                    return false;
                }
            }
        }
        return retorno;
    }
    
    public static boolean esStringCorrectoLetras(String aVerificar) {
        boolean retorno = true;
        for (int i = 0; i < aVerificar.length(); i++) {
            char aVerificarEnI = aVerificar.charAt(i);
            int let = (int) aVerificarEnI;
            //Si no esta dentro de lo correcto sea correcto solo letras (a-z,A-Z)
            if (!((let > 64 && let < 91) || (let > 96 && let < 123))) {
                //Ñ, ñ, tildes
                if (!((let==241)||(let==209)||(let==225)||(let==233)||(let==237)||(let==243)||(let==250)
                        ||(let==193)||(let==201)||(let==205)||(let==211)||(let==218))) {
                    return false;
                }
            }
        }
        return retorno;
    }
    
    public static boolean esStringCorrectoNumeros(String aVerificar) {
        boolean retorno = true;
        for (int i = 0; i < aVerificar.length(); i++) {
            char aVerificarEnI = aVerificar.charAt(i);
            int let = (int) aVerificarEnI;
            //Si no esta dentro de lo correcto sea correcto solo numeros (0-9)
            if (!((let > 47 && let < 58))) {
                return false;
            }
        }
        return retorno;
    }
    
    //Ayuda para parseo a int de fechas
    public static int formateoParaInt(String aFormatear) {
        if (aFormatear.startsWith("0")) {
            aFormatear = aFormatear.substring(1);
        }
        if (aFormatear.equals("ene")) {
            return 1;
        }
        if (aFormatear.equals("feb")) {
            return 2;
        }
        if (aFormatear.equals("mar")) {
            return 3;
        }
        if (aFormatear.equals("abr")) {
            return 4;
        }
        if (aFormatear.equals("may")) {
            return 5;
        }
        if (aFormatear.equals("jun")) {
            return 6;
        }
        if (aFormatear.equals("jul")) {
            return 7;
        }
        if (aFormatear.equals("ago")) {
            return 8;
        }
        if (aFormatear.equals("sep")) {
            return 9;
        }
        if (aFormatear.equals("oct")) {
            return 10;
        }
        if (aFormatear.equals("nov")) {
            return 11;
        }
        if (aFormatear.equals("dic")) {
            return 12;
        }
        if (aFormatear.equals("")) {
            return 0;
        }
        return Integer.parseInt(aFormatear);
    }
    
    public static boolean comparacionFechasCorrecto(String[] fecha1, String[] fecha2, int añoMenor, int añoMayor) {
        int dia = Logica.formateoParaInt(fecha1[0]);
        int mes = Logica.formateoParaInt(fecha1[1]);
        int año = Logica.formateoParaInt(fecha1[2]);
        int diaActual = Logica.formateoParaInt(fecha2[0]);
        int mesActual = Logica.formateoParaInt(fecha2[1]);
        int añoActual = Logica.formateoParaInt(fecha2[2]);
        
        boolean retornoMenor = (añoActual - año > añoMenor);
        if (añoActual - año == añoMenor) {
            retornoMenor = (mesActual > mes);
            if (mesActual == mes) {
                retornoMenor = (diaActual >= dia);
            }
        }

        boolean retornoMayor = (añoActual - año < añoMayor);

        return retornoMenor && retornoMayor;
    }
    
    public static String quitarEspaciosIniciales(String aCambiar) {
        int indiceEspacioFinal = -1;
        boolean yaNoEsEspacio = false;
        for (int i = 0; i < aCambiar.length() && !yaNoEsEspacio; i++) {
            char aCambiarEnI = aCambiar.charAt(i);
            int let = (int) aCambiarEnI;
            if (let==32) {
                indiceEspacioFinal++;
            } else {
                yaNoEsEspacio = true;
            }
        }
        if (indiceEspacioFinal != -1) {
            aCambiar = aCambiar.substring(indiceEspacioFinal + 1, aCambiar.length());
        }
        return aCambiar;
    }
    
    //Para el filechooser
    public final static String jpeg = "jpeg";
    public final static String jpg = "jpg";
    public final static String gif = "gif";
    public final static String tiff = "tiff";
    public final static String tif = "tif";
    public final static String png = "png";
    public final static String bmp = "bmp";

    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
}
