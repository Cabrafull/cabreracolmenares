//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Usuario extends Cuenta {

    //Atributos
    private String nacionalidad;
    private ArrayList<String> listaPreferencias;
    private ArrayList<String> listaRestricciones;
    private ArrayList<AlimentoIngerido> listaAlimentosIngeridos;
    private ArrayList<Consulta> listaConsultas;
    private ArrayList<PlanAlimentacion> listaPlanesAlimentacion;

    //Constructores
    public Usuario() {
        super();
        this.nacionalidad = "";
        this.listaPreferencias = new ArrayList<>();
        this.listaRestricciones = new ArrayList<>();
        this.listaAlimentosIngeridos = new ArrayList<>();
        this.listaConsultas = new ArrayList<>();
        this.listaPlanesAlimentacion = new ArrayList<>();
    }

    public Usuario(String laCuenta, BufferedImage laFotoPerfil, String elNombre, String elNombre2,
             String elApellido, String elApellido2) {
        super(laCuenta, laFotoPerfil, elNombre, elNombre2, elApellido, elApellido2);
        this.setNacionalidad("");
        this.listaPreferencias = new ArrayList<>();
        this.listaRestricciones = new ArrayList<>();
        this.listaAlimentosIngeridos = new ArrayList<>();
        this.listaConsultas = new ArrayList<>();
        this.listaPlanesAlimentacion = new ArrayList<>();
    }

    //Gets y Sets
    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String unaNacionalidad) {
        this.nacionalidad = unaNacionalidad;
    }
    
    public ArrayList<String> getListaPreferencias() {
        return listaPreferencias;
    }

    public void setListaPreferencias(ArrayList<String> unaListaPreferencias) {
        this.listaPreferencias = unaListaPreferencias;
    }

    public ArrayList<String> getListaRestricciones() {
        return listaRestricciones;
    }

    public void setListaRestricciones(ArrayList<String> unaListaRestricciones) {
        this.listaRestricciones = unaListaRestricciones;
    }

    public ArrayList<AlimentoIngerido> getListaAlimentosIngeridos() {
        return listaAlimentosIngeridos;
    }

    public void setListaAlimentosIngeridos(ArrayList<AlimentoIngerido> unaListaAlimentosIngeridos) {
        this.listaAlimentosIngeridos = unaListaAlimentosIngeridos;
    }

    public ArrayList<Consulta> getListaConsultas() {
        return listaConsultas;
    }

    public void setListaConsultas(ArrayList<Consulta> unaListaConsultas) {
        this.listaConsultas = unaListaConsultas;
    }

    public ArrayList<PlanAlimentacion> getListaPlanesAlimentacion() {
        return listaPlanesAlimentacion;
    }

    public void setListaPlanesAlimentacion(ArrayList<PlanAlimentacion> unaListaPlanesAlimentacion) {
        this.listaPlanesAlimentacion = unaListaPlanesAlimentacion;
    }

    //Redefinir el ToString
    @Override
    public String toString() {
        String retorno = "";
        if (!this.getNombre().equals("")) {
            retorno = retorno + this.getNombre() + " ";
        }
        if (!this.getNombre2().equals("")) {
            retorno = retorno + this.getNombre2() + " ";
        }
        if (!this.getApellido().equals("")) {
            retorno = retorno + this.getApellido() + " ";
        }
        if (!this.getApellido2().equals("")) {
            retorno = retorno + this.getApellido2();
        }
        return retorno;
    }
}
