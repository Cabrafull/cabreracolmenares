//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

public class Nutriente {

    //Atributos
    private String nombre;
    private float proporcion;

    //Constructores
    public Nutriente() {
        this.nombre = "";
        this.proporcion = 0;
    }

    public Nutriente(String unNombre, float unaProporcion) {
        this.setNombre(unNombre);
        this.setProporcion(unaProporcion);
    }

    //Gets y Sets
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }

    public float getProporcion() {
        return proporcion;
    }

    public void setProporcion(float unaProporcion) {
        this.proporcion = unaProporcion;
    }

    //Redefinir el ToString
    @Override
    public String toString() {
        return this.getNombre();
    }

    //Redefinir el equals
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        return this.getNombre().equalsIgnoreCase(((Nutriente) o).getNombre());
    }
}
