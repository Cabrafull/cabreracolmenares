//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

public class AlimentoIngerido {

    //Atributos
    private Alimento alimentoIngerido;
    private String fechaDeIngerido;

    //Constructores
    public AlimentoIngerido() {
        this.alimentoIngerido = null;
        this.fechaDeIngerido = "";
    }

    public AlimentoIngerido(Alimento unAlimentoIngerido, String unaFechaDeIngerido) {
        this.setAlimentoIngerido(unAlimentoIngerido);
        this.setFechaDeIngerido(unaFechaDeIngerido);
    }

    //Gets y Sets
    public Alimento getAlimentoIngerido() {
        return alimentoIngerido;
    }

    public void setAlimentoIngerido(Alimento unAlimentoIngerido) {
        this.alimentoIngerido = unAlimentoIngerido;
    }

    public String getFechaDeIngerido() {
        return fechaDeIngerido;
    }

    public void setFechaDeIngerido(String unaFechaDeIngerido) {
        this.fechaDeIngerido = unaFechaDeIngerido;
    }

    //Redefinir el ToString
    @Override
    public String toString() {
        return this.getAlimentoIngerido().toString();
    }

}
