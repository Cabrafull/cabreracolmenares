//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class SistemaTest {
    
    public SistemaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getListaUsuarios method, of class Sistema.
     */
    @Test
    public void testGetListaUsuarios() {
        Sistema instance = new Sistema();
        ArrayList<Usuario> expResult = new ArrayList<>();
        ArrayList<Usuario> result = instance.getListaUsuarios();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaUsuarios method, of class Sistema.
     */
    @Test
    public void testSetListaUsuarios() {
        ArrayList<Usuario> unaListaUsuarios = new ArrayList<>();
        Sistema instance = new Sistema();
        instance.setListaUsuarios(unaListaUsuarios);
        assertEquals(unaListaUsuarios, instance.getListaUsuarios());
    }

    /**
     * Test of getListaProfesionales method, of class Sistema.
     */
    @Test
    public void testGetListaProfesionales() {
        Sistema instance = new Sistema();
        ArrayList<Profesional> expResult = new ArrayList<>();
        ArrayList<Profesional> result = instance.getListaProfesionales();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaProfesionales method, of class Sistema.
     */
    @Test
    public void testSetListaProfesionales() {
        ArrayList<Profesional> unaListaProfesionales = new ArrayList<>();
        Sistema instance = new Sistema();
        instance.setListaProfesionales(unaListaProfesionales);
        assertEquals(unaListaProfesionales, instance.getListaProfesionales());
    }

    /**
     * Test of getListaAlimentos method, of class Sistema.
     */
    @Test
    public void testGetListaAlimentos() {
        Sistema instance = new Sistema();
        ArrayList<Alimento> expResult = new ArrayList<>();
        ArrayList<Alimento> result = instance.getListaAlimentos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaAlimentos method, of class Sistema.
     */
    @Test
    public void testSetListaAlimentos() {
        ArrayList<Alimento> unaListaAlimentos = new ArrayList<>();
        Sistema instance = new Sistema();
        instance.setListaAlimentos(unaListaAlimentos);
        assertEquals(unaListaAlimentos, instance.getListaAlimentos());
    }

    /**
     * Test of hayMismaCuentaUsuario method, of class Sistema.
     */
    @Test
    public void testHayMismaCuentaUsuario() {
        String cuenta = "";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.hayMismaCuentaUsuario(cuenta);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testHayMismaCuentaUsuarioExiste() {
        String cuenta = "";
        Sistema instance = new Sistema();
        Usuario nuevoUsuario = new Usuario();
        instance.getListaUsuarios().add(nuevoUsuario);
        boolean expResult = true;
        boolean result = instance.hayMismaCuentaUsuario(cuenta);
        assertEquals(expResult, result);
    }

    /**
     * Test of hayMismaCuentaProfesional method, of class Sistema.
     */
    @Test
    public void testHayMismaCuentaProfesional() {
        String cuenta = "";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.hayMismaCuentaProfesional(cuenta);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testHayMismaCuentaProfesionalExiste() {
        String cuenta = "";
        Sistema instance = new Sistema();
        Profesional nuevoProfesional = new Profesional();
        instance.getListaProfesionales().add(nuevoProfesional);
        boolean expResult = true;
        boolean result = instance.hayMismaCuentaProfesional(cuenta);
        assertEquals(expResult, result);
    }

    /**
     * Test of hayMismaCuentaSistema method, of class Sistema.
     */
    @Test
    public void testHayMismaCuentaSistema() {
        String cuenta = "";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.hayMismaCuentaSistema(cuenta);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAlimentoConNombre method, of class Sistema.
     */
    @Test
    public void testGetAlimentoConNombre() {
        String nombreAlimento = "";
        Sistema instance = new Sistema();
        Alimento expResult = null;
        Alimento result = instance.getAlimentoConNombre(nombreAlimento);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetAlimentoConNombreExiste() {
        String nombreAlimento = "";
        Sistema instance = new Sistema();
        Alimento expResult = new Alimento();
        instance.getListaAlimentos().add(expResult);
        Alimento result = instance.getAlimentoConNombre(nombreAlimento);
        assertEquals(expResult, result);
    }
    
}
