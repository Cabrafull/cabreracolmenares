//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class NutrienteTest {
    
    public NutrienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /*Constructor con parametros*/
    @Test
    public void testNutriente() {
        Nutriente instance = new Nutriente("",0);
        boolean expResult = true;
        boolean result = instance.getNombre().equals("") && instance.getProporcion()==0;
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getNombre method, of class Nutriente.
     */
    @Test
    public void testGetNombre() {
        Nutriente instance = new Nutriente();
        String expResult = "";
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Nutriente.
     */
    @Test
    public void testSetNombre() {
        String unNombre = "";
        Nutriente instance = new Nutriente();
        instance.setNombre(unNombre);
        assertEquals(unNombre, instance.getNombre());
    }

    /**
     * Test of getProporcion method, of class Nutriente.
     */
    @Test
    public void testGetProporcion() {
        Nutriente instance = new Nutriente();
        float expResult = 0.0F;
        float result = instance.getProporcion();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setProporcion method, of class Nutriente.
     */
    @Test
    public void testSetProporcion() {
        float unaProporcion = 0.0F;
        Nutriente instance = new Nutriente();
        instance.setProporcion(unaProporcion);
        assertEquals(unaProporcion, instance.getProporcion(), 0.0);
    }

    /**
     * Test of toString method, of class Nutriente.
     */
    @Test
    public void testToString() {
        Nutriente instance = new Nutriente();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Nutriente.
     */
    @Test
    public void testEquals() {
        Object o = null;
        Nutriente instance1 = new Nutriente();
        Nutriente instance2 = new Nutriente();
        boolean comparacion = instance1.equals(instance2);
        boolean enNull = instance1.equals(o);
        boolean expResult = true;
        boolean result = comparacion && !enNull;
        assertEquals(expResult, result);
    }
    
}
