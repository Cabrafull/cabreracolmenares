//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProfesionalTest {
    
    public ProfesionalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /*Constructor con parametros*/
    @Test
    public void testProfesional() {
        Profesional instance = new Profesional("", null, "", "", "", "");
        boolean expResult = true;
        boolean result = instance.getCuenta().equals("") && instance.getFotoPerfil()==null
                && instance.getNombre().equals("") && instance.getNombre2().equals("")
                    && instance.getApellido().equals("") && instance.getApellido2().equals("")
                        && instance.getFechaDeNacimiento().equals("") && instance.getNombreTituloProfesional().equals("")
                            && instance.getFechaDeGraduacion().equals("")
                                && instance.getListaConsultasNoContestadas().equals(new ArrayList<>());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getNombreTituloProfesional method, of class Profesional.
     */
    @Test
    public void testGetNombreTituloProfesional() {
        Profesional instance = new Profesional();
        String expResult = "";
        String result = instance.getNombreTituloProfesional();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombreTituloProfesional method, of class Profesional.
     */
    @Test
    public void testSetNombreTituloProfesional() {
        String unNombreTituloProfesional = "";
        Profesional instance = new Profesional();
        instance.setNombreTituloProfesional(unNombreTituloProfesional);
        assertEquals(unNombreTituloProfesional, instance.getNombreTituloProfesional());
    }

    /**
     * Test of getFechaDeGraduacion method, of class Profesional.
     */
    @Test
    public void testGetFechaDeGraduacion() {
        Profesional instance = new Profesional();
        String expResult = "";
        String result = instance.getFechaDeGraduacion();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaDeGraduacion method, of class Profesional.
     */
    @Test
    public void testSetFechaDeGraduacion() {
        String unaFechaDeGraduacion = "";
        Profesional instance = new Profesional();
        instance.setFechaDeGraduacion(unaFechaDeGraduacion);
        assertEquals(unaFechaDeGraduacion, instance.getFechaDeGraduacion());
    }

    /**
     * Test of getPaisDondeObtuvoTitulo method, of class Profesional.
     */
    @Test
    public void testGetPaisDondeObtuvoTitulo() {
        Profesional instance = new Profesional();
        String expResult = "";
        String result = instance.getPaisDondeObtuvoTitulo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPaisDondeObtuvoTitulo method, of class Profesional.
     */
    @Test
    public void testSetPaisDondeObtuvoTitulo() {
        String unPaisDondeObtuvoTitulo = "";
        Profesional instance = new Profesional();
        instance.setPaisDondeObtuvoTitulo(unPaisDondeObtuvoTitulo);
        assertEquals(unPaisDondeObtuvoTitulo, instance.getPaisDondeObtuvoTitulo());
    }

    /**
     * Test of getListaConsultasNoContestadas method, of class Profesional.
     */
    @Test
    public void testGetListaConsultasNoContestadas() {
        Profesional instance = new Profesional();
        ArrayList<Consulta> expResult = new ArrayList<>();
        ArrayList<Consulta> result = instance.getListaConsultasNoContestadas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaConsultasNoContestadas method, of class Profesional.
     */
    @Test
    public void testSetListaConsultasNoContestadas() {
        ArrayList<Consulta> unaListaConsultasNoContestadas = new ArrayList<>();;
        Profesional instance = new Profesional();
        instance.setListaConsultasNoContestadas(unaListaConsultasNoContestadas);
        assertEquals(unaListaConsultasNoContestadas, instance.getListaConsultasNoContestadas());
    }

    /**
     * Test of toString method, of class Profesional.
     */
    @Test
    public void testToString() {
        Profesional instance = new Profesional();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method 2, of class Profesional.
     */
    @Test
    public void testToStringNoVacios() {
        Profesional instance = new Profesional();
        instance.setNombre("n1");
        instance.setNombre2("n2");
        instance.setApellido("a1");
        instance.setApellido2("a2");
        String expResult = "n1 n2 a1 a2";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
