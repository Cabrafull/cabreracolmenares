//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class AlimentoIngeridoTest {
    
    public AlimentoIngeridoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /*Constructor con parametros*/
    @Test
    public void testAlimentoIngerido() {
        Alimento nuevoAlimento = new Alimento();
        AlimentoIngerido instance = new AlimentoIngerido(nuevoAlimento,"");
        boolean expResult = true;
        boolean result = instance.getAlimentoIngerido().equals(nuevoAlimento) && instance.getFechaDeIngerido().equals("");
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAlimentoIngerido method, of class AlimentoIngerido.
     */
    @Test
    public void testGetAlimentoIngerido() {
        AlimentoIngerido instance = new AlimentoIngerido();
        String expResult = "";
        String result = instance.getFechaDeIngerido();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAlimentoIngerido method, of class AlimentoIngerido.
     */
    @Test
    public void testSetAlimentoIngerido() {
        Alimento unAlimentoIngerido = null;
        AlimentoIngerido instance = new AlimentoIngerido();
        instance.setAlimentoIngerido(unAlimentoIngerido);
        assertEquals(null, instance.getAlimentoIngerido());
    }

    /**
     * Test of getFechaDeIngerido method, of class AlimentoIngerido.
     */
    @Test
    public void testGetFechaDeIngerido() {
        AlimentoIngerido instance = new AlimentoIngerido();
        String expResult = "";
        String result = instance.getFechaDeIngerido();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaDeIngerido method, of class AlimentoIngerido.
     */
    @Test
    public void testSetFechaDeIngerido() {
        String unaFechaDeIngerido = "";
        AlimentoIngerido instance = new AlimentoIngerido();
        instance.setFechaDeIngerido(unaFechaDeIngerido);
        assertEquals("", instance.getFechaDeIngerido());
    }

    /**
     * Test of toString method, of class AlimentoIngerido.
     */
    @Test
    public void testToString() {
        AlimentoIngerido instance = new AlimentoIngerido(new Alimento("",""), "");
        String expResult = " ()";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
