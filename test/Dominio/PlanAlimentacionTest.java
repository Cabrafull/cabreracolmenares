//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PlanAlimentacionTest {
    
    public PlanAlimentacionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /*Constructor con parametros*/
    @Test
    public void testPlanAlimentacion() {
        PlanAlimentacion instance = new PlanAlimentacion(null, "");
        boolean expResult = true;
        boolean result = instance.getNombre().equals("") && instance.getAlimentos()==null;
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAlimentos method, of class PlanAlimentacion.
     */
    @Test
    public void testGetAlimentos() {
        PlanAlimentacion instance = new PlanAlimentacion();
        Alimento[][] expResult = new Alimento[7][4];
        Alimento[][] result = instance.getAlimentos();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of setAlimentos method, of class PlanAlimentacion.
     */
    @Test
    public void testSetAlimentos() {
        Alimento[][] losAlimentos = null;
        PlanAlimentacion instance = new PlanAlimentacion();
        instance.setAlimentos(losAlimentos);
        assertArrayEquals(losAlimentos, instance.getAlimentos());
    }

    /**
     * Test of getNombre method, of class PlanAlimentacion.
     */
    @Test
    public void testGetNombre() {
        PlanAlimentacion instance = new PlanAlimentacion();
        String expResult = "";
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class PlanAlimentacion.
     */
    @Test
    public void testSetNombre() {
        String elNombre = "";
        PlanAlimentacion instance = new PlanAlimentacion();
        instance.setNombre(elNombre);
        assertEquals(elNombre, instance.getNombre());
    }

    /**
     * Test of toString method, of class PlanAlimentacion.
     */
    @Test
    public void testToString() {
        PlanAlimentacion instance = new PlanAlimentacion();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
