//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class AlimentoTest {
    
    public AlimentoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /*Constructor con parametros*/
    @Test
    public void testAlimento() {
        Alimento instance = new Alimento("","");
        boolean expResult = true;
        boolean result = instance.getNombre().equals("") && instance.getTipoDeAlimento().equals("") &&
                instance.getListaNutrientesPrincipales().equals(new ArrayList<>());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getNombre method, of class Alimento.
     */
    @Test
    public void testGetNombre() {
        Alimento instance = new Alimento();
        String expResult = "";
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Alimento.
     */
    @Test
    public void testSetNombre() {
        String unNombre = "";
        Alimento instance = new Alimento();
        instance.setNombre(unNombre);
        assertEquals(unNombre, instance.getNombre());
    }

    /**
     * Test of getTipoDeAlimento method, of class Alimento.
     */
    @Test
    public void testGetTipoDeAlimento() {
        Alimento instance = new Alimento();
        String expResult = "";
        String result = instance.getTipoDeAlimento();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTipoDeAlimento method, of class Alimento.
     */
    @Test
    public void testSetTipoDeAlimento() {
        String unTipoDeAlimento = "";
        Alimento instance = new Alimento();
        instance.setTipoDeAlimento(unTipoDeAlimento);
        assertEquals(unTipoDeAlimento, instance.getTipoDeAlimento());
    }

    /**
     * Test of getListaNutrientesPrincipales method, of class Alimento.
     */
    @Test
    public void testGetListaNutrientesPrincipales() {
        Alimento instance = new Alimento();
        ArrayList<Nutriente> expResult = new ArrayList<>();
        ArrayList<Nutriente> result = instance.getListaNutrientesPrincipales();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaNutrientesPrincipales method, of class Alimento.
     */
    @Test
    public void testSetListaNutrientesPrincipales() {
        ArrayList<Nutriente> unaListaNutrientesPrincipales = new ArrayList<>();
        Alimento instance = new Alimento();
        instance.setListaNutrientesPrincipales(unaListaNutrientesPrincipales);
        assertEquals(unaListaNutrientesPrincipales, instance.getListaNutrientesPrincipales());
    }

    /**
     * Test of toString method, of class Alimento.
     */
    @Test
    public void testToString() {
        Alimento instance = new Alimento();
        String expResult = " ()";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Alimento.
     */
    @Test
    public void testEquals() {
        Object o = null;
        Alimento instance1 = new Alimento();
        Alimento instance2 = new Alimento();
        boolean comparacion = instance1.equals(instance2);
        boolean enNull = instance1.equals(o);
        boolean expResult = true;
        boolean result = comparacion && !enNull;
        assertEquals(expResult, result);
    }
    
}
