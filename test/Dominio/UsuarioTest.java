//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class UsuarioTest {
    
    public UsuarioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /*Constructor con parametros*/
    @Test
    public void testUsuario() {
        Usuario instance = new Usuario("", null, "", "", "", "");
        boolean expResult = true;
        boolean result = instance.getCuenta().equals("") && instance.getFotoPerfil()==null
                && instance.getNombre().equals("") && instance.getNombre2().equals("")
                    && instance.getApellido().equals("") && instance.getApellido2().equals("")
                        && instance.getFechaDeNacimiento().equals("") && instance.getNacionalidad().equals("")
                            && instance.getListaPreferencias().equals(new ArrayList<>())
                                && instance.getListaRestricciones().equals(new ArrayList<>())
                                    && instance.getListaAlimentosIngeridos().equals(new ArrayList<>())
                                        && instance.getListaConsultas().equals(new ArrayList<>())
                                            && instance.getListaPlanesAlimentacion().equals(new ArrayList<>());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getNacionalidad method, of class Usuario.
     */
    @Test
    public void testGetNacionalidad() {
        Usuario instance = new Usuario();
        String expResult = "";
        String result = instance.getNacionalidad();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNacionalidad method, of class Usuario.
     */
    @Test
    public void testSetNacionalidad() {
        String unaNacionalidad = "";
        Usuario instance = new Usuario();
        instance.setNacionalidad(unaNacionalidad);
        assertEquals(unaNacionalidad, instance.getNacionalidad());
    }

    /**
     * Test of getListaPreferencias method, of class Usuario.
     */
    @Test
    public void testGetListaPreferencias() {
        Usuario instance = new Usuario();
        ArrayList<String> expResult = new ArrayList<>();
        ArrayList<String> result = instance.getListaPreferencias();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaPreferencias method, of class Usuario.
     */
    @Test
    public void testSetListaPreferencias() {
        ArrayList<String> unaListaPreferencias = new ArrayList<>();
        Usuario instance = new Usuario();
        instance.setListaPreferencias(unaListaPreferencias);
        assertEquals(unaListaPreferencias, instance.getListaPreferencias());
    }

    /**
     * Test of getListaRestricciones method, of class Usuario.
     */
    @Test
    public void testGetListaRestricciones() {
        Usuario instance = new Usuario();
        ArrayList<String> expResult = new ArrayList<>();
        ArrayList<String> result = instance.getListaRestricciones();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaRestricciones method, of class Usuario.
     */
    @Test
    public void testSetListaRestricciones() {
        ArrayList<String> unaListaRestricciones = new ArrayList<>();
        Usuario instance = new Usuario();
        instance.setListaRestricciones(unaListaRestricciones);
        assertEquals(unaListaRestricciones, instance.getListaRestricciones());
    }

    /**
     * Test of getListaAlimentosIngeridos method, of class Usuario.
     */
    @Test
    public void testGetListaAlimentosIngeridos() {
        Usuario instance = new Usuario();
        ArrayList<AlimentoIngerido> expResult = new ArrayList<>();
        ArrayList<AlimentoIngerido> result = instance.getListaAlimentosIngeridos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaAlimentosIngeridos method, of class Usuario.
     */
    @Test
    public void testSetListaAlimentosIngeridos() {
        ArrayList<AlimentoIngerido> unaListaAlimentosIngeridos = new ArrayList<>();
        Usuario instance = new Usuario();
        instance.setListaAlimentosIngeridos(unaListaAlimentosIngeridos);
        assertEquals(unaListaAlimentosIngeridos, instance.getListaAlimentosIngeridos());
    }

    /**
     * Test of getListaConsultas method, of class Usuario.
     */
    @Test
    public void testGetListaConsultas() {
        Usuario instance = new Usuario();
        ArrayList<Consulta> expResult = new ArrayList<>();
        ArrayList<Consulta> result = instance.getListaConsultas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaConsultas method, of class Usuario.
     */
    @Test
    public void testSetListaConsultas() {
        ArrayList<Consulta> unaListaConsultas = new ArrayList<>();
        Usuario instance = new Usuario();
        instance.setListaConsultas(unaListaConsultas);
        assertEquals(unaListaConsultas, instance.getListaConsultas());
    }

    /**
     * Test of getListaPlanesAlimentacion method, of class Usuario.
     */
    @Test
    public void testGetListaPlanesAlimentacion() {
        Usuario instance = new Usuario();
        ArrayList<PlanAlimentacion> expResult = new ArrayList<>();
        ArrayList<PlanAlimentacion> result = instance.getListaPlanesAlimentacion();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaPlanesAlimentacion method, of class Usuario.
     */
    @Test
    public void testSetListaPlanesAlimentacion() {
        ArrayList<PlanAlimentacion> unaListaPlanesAlimentacion = new ArrayList<>();
        Usuario instance = new Usuario();
        instance.setListaPlanesAlimentacion(unaListaPlanesAlimentacion);
        assertEquals(unaListaPlanesAlimentacion, instance.getListaPlanesAlimentacion());
    }

    /**
     * Test of toString method, of class Usuario.
     */
    @Test
    public void testToString() {
        Usuario instance = new Usuario();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method 2, of class Usuario.
     */
    @Test
    public void testToStringNoVacios() {
        Usuario instance = new Usuario();
        instance.setNombre("n1");
        instance.setNombre2("n2");
        instance.setApellido("a1");
        instance.setApellido2("a2");
        String expResult = "n1 n2 a1 a2";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
