//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.io.File;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class LogicaTest {
    
    public LogicaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of esStringCorrecto method, of class Logica.
     */
    @Test
    public void testEsStringCorrecto() {
        String aVerificar = "";
        boolean expResult = true;
        boolean result = Logica.esStringCorrecto(aVerificar);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringCorrectoNoVacio() {
        String aVerificar = "qwertyuiopasdfghjklñzxcvbnmQWERTYUIOPASDFGHJKLÑZXCVBNMáéíóúÁÉÍÓÚ0123456789";
        boolean expResult = true;
        boolean result = Logica.esStringCorrecto(aVerificar);
        assertEquals(expResult, result);
    }

    /**
     * Test of esStringCorrectoLetras method, of class Logica.
     */
    @Test
    public void testEsStringCorrectoLetras() {
        String aVerificar = "";
        boolean expResult = true;
        boolean result = Logica.esStringCorrectoLetras(aVerificar);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringCorrectoLetrasNoVacio() {
        String aVerificar = "qwertyuiopasdfghjklñzxcvbnmQWERTYUIOPASDFGHJKLÑZXCVBNMáéíóúÁÉÍÓÚ";
        boolean expResult = true;
        boolean result = Logica.esStringCorrectoLetras(aVerificar);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringCorrectoLetrasConNumeros() {
        String aVerificar = "0123456789";
        boolean expResult = false;
        boolean result = Logica.esStringCorrectoLetras(aVerificar);
        assertEquals(expResult, result);
    }

    /**
     * Test of esStringCorrectoNumeros method, of class Logica.
     */
    @Test
    public void testEsStringCorrectoNumeros() {
        String aVerificar = "";
        boolean expResult = true;
        boolean result = Logica.esStringCorrectoNumeros(aVerificar);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringCorrectoNumerosNoVacio() {
        String aVerificar = "0123456789";
        boolean expResult = true;
        boolean result = Logica.esStringCorrectoNumeros(aVerificar);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEsStringCorrectoNumerosConLetras() {
        String aVerificar = "qwertyuiopasdfghjklñzxcvbnmQWERTYUIOPASDFGHJKLÑZXCVBNMáéíóúÁÉÍÓÚ";
        boolean expResult = false;
        boolean result = Logica.esStringCorrectoNumeros(aVerificar);
        assertEquals(expResult, result);
    }

    /**
     * Test of formateoParaInt method, of class Logica.
     */
    @Test
    public void testFormateoParaInt() {
        String aFormatear = "";
        int expResult = 0;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntCero() {
        String aFormatear = "09";
        int expResult = 9;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntEne() {
        String aFormatear = "ene";
        int expResult = 1;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntFeb() {
        String aFormatear = "feb";
        int expResult = 2;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntMar() {
        String aFormatear = "mar";
        int expResult = 3;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntAbr() {
        String aFormatear = "abr";
        int expResult = 4;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntMay() {
        String aFormatear = "may";
        int expResult = 5;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntJun() {
        String aFormatear = "jun";
        int expResult = 6;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntJul() {
        String aFormatear = "jul";
        int expResult = 7;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntAgo() {
        String aFormatear = "ago";
        int expResult = 8;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntSep() {
        String aFormatear = "sep";
        int expResult = 9;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntOct() {
        String aFormatear = "oct";
        int expResult = 10;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntNov() {
        String aFormatear = "nov";
        int expResult = 11;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFormateoParaIntDic() {
        String aFormatear = "dic";
        int expResult = 12;
        int result = Logica.formateoParaInt(aFormatear);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of comparacionFechasCorrecto method, of class Logica.
     */
    @Test
    public void testComparacionFechasCorrecto() {
        String[] fecha1 = new String[3];
        fecha1[0] = "0";
        fecha1[1] = "0";
        fecha1[2] = "0";
        String[] fecha2 = new String[3];
        fecha2[0] = "0";
        fecha2[1] = "0";
        fecha2[2] = "0";
        int añoMenor = 0;
        int añoMayor = 0;
        boolean expResult = false;
        boolean result = Logica.comparacionFechasCorrecto(fecha1, fecha2, añoMenor, añoMayor);
        assertEquals(expResult, result);
    }

    /**
     * Test of quitarEspaciosIniciales method, of class Logica.
     */
    @Test
    public void testQuitarEspaciosIniciales() {
        String aCambiar = "             a";
        String expResult = "a";
        String result = Logica.quitarEspaciosIniciales(aCambiar);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testQuitarEspaciosInicialesVacio() {
        String aCambiar = "";
        String expResult = "";
        String result = Logica.quitarEspaciosIniciales(aCambiar);
        assertEquals(expResult, result);
    }

    /**
     * Test of getExtension method, of class Logica.
     */
    @Test
    public void testGetExtension() {
        File f = null;
        String expResult = "";
        String result = Logica.getExtension(f);
        assertEquals(expResult, result);
    }
    
}
