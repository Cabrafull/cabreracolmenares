//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import java.awt.image.BufferedImage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CuentaTest {
    
    public CuentaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /*Constructor con parametros*/
    @Test
    public void testCuenta() {
        Cuenta instance = new Cuenta("", null, "", "", "", "");
        boolean expResult = true;
        boolean result = instance.getCuenta().equals("") && instance.getFotoPerfil()==null
                && instance.getNombre().equals("") && instance.getNombre2().equals("")
                    && instance.getApellido().equals("") && instance.getApellido2().equals("")
                        && instance.getFechaDeNacimiento().equals("");
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getCuenta method, of class Cuenta.
     */
    @Test
    public void testGetCuenta() {
        Cuenta instance = new Cuenta();
        String expResult = "";
        String result = instance.getCuenta();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCuenta method, of class Cuenta.
     */
    @Test
    public void testSetCuenta() {
        String unaCuenta = "";
        Cuenta instance = new Cuenta();
        instance.setCuenta(unaCuenta);
        assertEquals(unaCuenta, instance.getCuenta());
    }

    /**
     * Test of getNombre method, of class Cuenta.
     */
    @Test
    public void testGetNombre() {
        Cuenta instance = new Cuenta();
        String expResult = "";
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Cuenta.
     */
    @Test
    public void testSetNombre() {
        String unNombre = "";
        Cuenta instance = new Cuenta();
        instance.setNombre(unNombre);
        assertEquals(unNombre, instance.getNombre());
    }

    /**
     * Test of getNombre2 method, of class Cuenta.
     */
    @Test
    public void testGetNombre2() {
        Cuenta instance = new Cuenta();
        String expResult = "";
        String result = instance.getNombre2();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre2 method, of class Cuenta.
     */
    @Test
    public void testSetNombre2() {
        String unNombre2 = "";
        Cuenta instance = new Cuenta();
        instance.setNombre2(unNombre2);
        assertEquals(unNombre2, instance.getNombre2());
    }

    /**
     * Test of getApellido method, of class Cuenta.
     */
    @Test
    public void testGetApellido() {
        Cuenta instance = new Cuenta();
        String expResult = "";
        String result = instance.getApellido();
        assertEquals(expResult, result);
    }

    /**
     * Test of setApellido method, of class Cuenta.
     */
    @Test
    public void testSetApellido() {
        String unApellido = "";
        Cuenta instance = new Cuenta();
        instance.setApellido(unApellido);
        assertEquals(unApellido, instance.getApellido());
    }

    /**
     * Test of getApellido2 method, of class Cuenta.
     */
    @Test
    public void testGetApellido2() {
        Cuenta instance = new Cuenta();
        String expResult = "";
        String result = instance.getApellido2();
        assertEquals(expResult, result);
    }

    /**
     * Test of setApellido2 method, of class Cuenta.
     */
    @Test
    public void testSetApellido2() {
        String unApellido2 = "";
        Cuenta instance = new Cuenta();
        instance.setApellido2(unApellido2);
        assertEquals(unApellido2, instance.getApellido2());
    }

    /**
     * Test of getFechaDeNacimiento method, of class Cuenta.
     */
    @Test
    public void testGetFechaDeNacimiento() {
        Cuenta instance = new Cuenta();
        String expResult = "";
        String result = instance.getFechaDeNacimiento();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaDeNacimiento method, of class Cuenta.
     */
    @Test
    public void testSetFechaDeNacimiento() {
        String unaFechaDeNacimiento = "";
        Cuenta instance = new Cuenta();
        instance.setFechaDeNacimiento(unaFechaDeNacimiento);
        assertEquals(unaFechaDeNacimiento, instance.getFechaDeNacimiento());
    }

    /**
     * Test of getFotoPerfil method, of class Cuenta.
     */
    @Test
    public void testGetFotoPerfil() {
        Cuenta instance = new Cuenta();
        BufferedImage expResult = null;
        BufferedImage result = instance.getFotoPerfil();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFotoPerfil method, of class Cuenta.
     */
    @Test
    public void testSetFotoPerfil() {
        BufferedImage laFotoPerfil = null;
        Cuenta instance = new Cuenta();
        instance.setFotoPerfil(laFotoPerfil);
        assertEquals(laFotoPerfil, instance.getFotoPerfil());
    }

    /**
     * Test of equals method, of class Cuenta.
     */
    @Test
    public void testEquals() {
        Object o = null;
        Cuenta instance1 = new Cuenta();
        Cuenta instance2 = new Cuenta();
        boolean comparacion = instance1.equals(instance2);
        boolean enNull = instance1.equals(o);
        boolean expResult = true;
        boolean result = comparacion && !enNull;
        assertEquals(expResult, result);
    }
    
}
