//Autor: Santiago Cabrera 203665 y Carlos Colmenares 201263

package Dominio;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConsultaTest {
    
    public ConsultaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /*Constructor con parametros*/
    @Test
    public void testConsulta() {
        Alimento nuevoAlimento = new Alimento();
        Usuario nuevoUsuario = new Usuario();
        Consulta instance = new Consulta("", false, nuevoAlimento, nuevoUsuario, "");
        boolean expResult = true;
        boolean result = instance.getConsulta().equals("") && !instance.getContestada()
                && instance.getAlimento().equals(nuevoAlimento) && instance.getCliente().equals(nuevoUsuario)
                    && instance.getIdProfesional().equals("");
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getConsulta method, of class Consulta.
     */
    @Test
    public void testGetConsulta() {
        Consulta instance = new Consulta();
        String expResult = "";
        String result = instance.getConsulta();
        assertEquals(expResult, result);
    }

    /**
     * Test of setConsulta method, of class Consulta.
     */
    @Test
    public void testSetConsulta() {
        String unaConsulta = "";
        Consulta instance = new Consulta();
        instance.setConsulta(unaConsulta);
        assertEquals(unaConsulta, instance.getConsulta());
    }

    /**
     * Test of getContestada method, of class Consulta.
     */
    @Test
    public void testGetContestada() {
        Consulta instance = new Consulta();
        boolean expResult = false;
        boolean result = instance.getContestada();
        assertEquals(expResult, result);
    }

    /**
     * Test of setContestada method, of class Consulta.
     */
    @Test
    public void testSetContestada() {
        boolean estaContestada = false;
        Consulta instance = new Consulta();
        instance.setContestada(estaContestada);
        assertEquals(estaContestada, instance.getContestada());
    }

    /**
     * Test of getAlimento method, of class Consulta.
     */
    @Test
    public void testGetAlimento() {
        Consulta instance = new Consulta();
        Alimento expResult = null;
        Alimento result = instance.getAlimento();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAlimento method, of class Consulta.
     */
    @Test
    public void testSetAlimento() {
        Alimento unAlimento = null;
        Consulta instance = new Consulta();
        instance.setAlimento(unAlimento);
        assertEquals(unAlimento, instance.getAlimento());
    }

    /**
     * Test of getCliente method, of class Consulta.
     */
    @Test
    public void testGetCliente() {
        Consulta instance = new Consulta();
        Usuario expResult = null;
        Usuario result = instance.getCliente();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCliente method, of class Consulta.
     */
    @Test
    public void testSetCliente() {
        Usuario unCliente = null;
        Consulta instance = new Consulta();
        instance.setCliente(unCliente);
        assertEquals(unCliente, instance.getCliente());
    }

    /**
     * Test of getIdProfesional method, of class Consulta.
     */
    @Test
    public void testGetIdProfesional() {
        Consulta instance = new Consulta();
        String expResult = "";
        String result = instance.getIdProfesional();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdProfesional method, of class Consulta.
     */
    @Test
    public void testSetIdProfesional() {
        String laIdProfesional = "";
        Consulta instance = new Consulta();
        instance.setIdProfesional(laIdProfesional);
        assertEquals(laIdProfesional, instance.getIdProfesional());
    }

    /**
     * Test of toString method, of class Consulta.
     */
    @Test
    public void testToString() {
        Consulta instance = new Consulta();
        instance.setAlimento(new Alimento());
        String expResult = "Alimento " + " por profesional ";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Consulta.
     */
    @Test
    public void testEquals() {
        Object o = null;
        Alimento alimento = new Alimento();
        Consulta instance1 = new Consulta();
        instance1.setAlimento(alimento);
        Consulta instance2 = new Consulta();
        instance2.setAlimento(alimento);
        boolean comparacion = instance1.equals(instance2);
        boolean enNull = instance1.equals(o);
        boolean expResult = true;
        boolean result = comparacion && !enNull;
        assertEquals(expResult, result);
    }
    
}
